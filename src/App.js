import React, {useState} from 'react'
import './App.scss'



function App(){

    const currentDate = () =>{
        let newDate = new Date()
        let date = newDate.getDate()
        let month = newDate.getMonth()+1
        let year = newDate.getFullYear()
        return `${date}/${month}/${year}`
    }
    

    const api = {
        key: "a4178a02f7d9e9d8c1a59e25b781dc8f",
        base: "https://api.openweathermap.org/data/2.5/"
      }

      const [items, setItems] = useState("")
      const [query, setQuery] = useState("")

      const handleSubmit = (e) => {
        console.log(`handle sumbit ${query}`)
        fetchItems()
        e.preventDefault()
      }
      console.log(`before query${query}`)
      
      
    //   useEffect( () =>{
    //       fetchItems()}, []
    //   )
      
      const fetchItems = async () =>{
          const res = await fetch(`${api.base}weather?q=${query}&units=metric&APPID=${api.key}`)
          const json = await res.json()
          console.log(json)
          setItems(json)
      }

    return(
        <React.Fragment>
            <div className="container">
                <div className= {`weather-wrapper ${typeof items.main !=="undefined" ? items.main.temp > 18 ? "summer" : "winter" : 'winter' }`}>
                    <form onSubmit={handleSubmit}>
                        <input type="text" placeholder="Search" value={query} onChange={e => setQuery(e.target.value)}  />
                        <button type="submit">Search</button>
                    </form>
                    <div className="weather-box">
                        <p className="city">{typeof items.name !=="undefined" ? items.name : 'Country'}</p>
                        <div className="temp">{typeof items.main !=="undefined" ? items.main.temp : 'Temp'} °c</div>
                        <p className="weather">{typeof items.main !=="undefined" ? items.main.temp > 18 ? "Summer" : "Winter" : 'Weather' }</p>
                        <p className="date">{currentDate()}</p>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default App